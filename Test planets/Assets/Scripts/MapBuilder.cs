﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBuilder : MonoBehaviour
{
    [SerializeField] private Planet planetPrefab;

    [SerializeField] private GameObject bottomLeftPoint;
    [SerializeField] private GameObject bottomRightPoint;
    [SerializeField] private GameObject ceilLeftPoint;

    [SerializeField] private PlanetParameters planetParameters;

    private const int minShipsOnPlanet = 1;
    private int maxShipsOnPlanet;
    private int maxCountOfPlanets;
    private float minPlanetDiameter;
    private float maxPlanetDiameter;
    private int currentPlanetsCount;

    private List<Planet> allPlanets = new List<Planet>();
    
    public void Init()
    {
        maxCountOfPlanets = planetParameters.maxCountOfPlanets;
        minPlanetDiameter = planetParameters.minPlanetDiameter;
        maxPlanetDiameter = planetParameters.maxPlanetDiameter;
        maxShipsOnPlanet = planetParameters.maxShipsOnPlanet;

        while (currentPlanetsCount < maxCountOfPlanets)
        {
            float x = Random.Range(bottomLeftPoint.transform.position.x + maxPlanetDiameter, bottomRightPoint.transform.position.x - maxPlanetDiameter);
            float z = Random.Range(bottomLeftPoint.transform.position.z + maxPlanetDiameter, ceilLeftPoint.transform.position.z - maxPlanetDiameter);
            float y = transform.position.y;
            Vector3 spawnPos = new Vector3(x, y, z);
            Vector3 spherePosHit = new Vector3(spawnPos.x, spawnPos.y + 10, spawnPos.z);
            
            RaycastHit hit;
            
            if (Physics.SphereCast(spherePosHit, maxPlanetDiameter, -transform.up, out hit, 10))
            {
                continue;
            }
            else
            {
                float randomDiameterPlanet = Random.Range(minPlanetDiameter, maxPlanetDiameter);
                int randomShipCount = Random.Range(minShipsOnPlanet, maxShipsOnPlanet);
                Planet tempPlanet = Instantiate(planetPrefab, spawnPos, Quaternion.identity);
                tempPlanet.Init(randomDiameterPlanet, randomShipCount);
                allPlanets.Add(tempPlanet);
                currentPlanetsCount++;
            }
        }
        
        SetupPlayerPlanet();
    }

    private void SetupPlayerPlanet()
    {
        int randomIndex = Random.Range(0, allPlanets.Count);
        allPlanets[randomIndex].SetupStartPlayer();
    }
}
