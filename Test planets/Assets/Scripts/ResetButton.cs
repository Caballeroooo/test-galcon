﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetButton : MonoBehaviour
{
    private void ResetGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void OnResetButton()
    {
        ResetGame();
    }
}
