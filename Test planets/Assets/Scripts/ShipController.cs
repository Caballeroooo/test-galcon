﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ShipController : MonoBehaviour
{
    [SerializeField] private Ship shipPrefab;

    private bool isTarget;

    private Planet firstPlanet;
    private Planet secondPlanet;

    void Update()
    {
        if (isTarget)
        {
            int shipCount = firstPlanet.GetHalfShipCount();

            for (int i = 0; i < shipCount; i++)
            {
                Ship tempShip = Instantiate(shipPrefab, firstPlanet.transform.position, Quaternion.identity);
                tempShip.StartMove(secondPlanet, secondPlanet.GetRadius());
            }

            isTarget = false;
        }
    }

    public void SendPlayerPlanetPos(Planet first)
    {
        firstPlanet = first;
    }

    public void SendNeutralPlanetPos(Planet second)
    {
        secondPlanet = second;
        isTarget = true;
    }
}
