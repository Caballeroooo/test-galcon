﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Planet : MonoBehaviour
{
    [SerializeField] private TextMesh shipsCount;
    [SerializeField] private PlanetParameters planetParameters;

    private static Planet currentPlayerPlanet;

    private static bool isPlayerPlanetSelected;
    private bool isPlayerPlanet;


    private MeshRenderer render;

    private Color blue = Color.blue;
    private Color cyan = Color.cyan;
    private Color gray = Color.gray;
    private Color white = Color.white;
    
    private int shipsCounter;
    private const int minShipsOnPlanet = 1;

    private float timer;
    private float planetDiameter;


    private void Update()
    {
        shipsCount.text = shipsCounter.ToString();
        
        if (isPlayerPlanet)
        {
            CountingShips();
        }

        if (shipsCounter <= 0)
            SetupPlayerPlanet();
    }

    public void Init(float diameter, int shipCount)
    {
        planetDiameter = diameter;
        render = GetComponent<MeshRenderer>();
        transform.localScale = new Vector3(diameter, diameter, diameter);
        shipsCounter = shipCount;
    }

    public void SetupStartPlayer()
    {
        SetupPlayerPlanet();
        shipsCounter = 50;
    }

    public int GetHalfShipCount()
    {
        shipsCounter /= 2;
        return shipsCounter;
    }

    public float GetRadius()
    {
        return planetDiameter / 2f;
    }

    public void ShipHit(int value)
    {
        if (!isPlayerPlanet)
        {
            shipsCounter -= value;
        }
        else
        {
            shipsCounter += value;
        }
    }
    
    private void SetupPlayerPlanet()
    {
        isPlayerPlanet = true;
        SetPlanetColor(blue);
    }
    
    private void CountingShips()
    {
        timer += Time.deltaTime;
        
        if (timer > 1f)
        {
            shipsCounter += 5;
            shipsCounter = Mathf.Clamp(shipsCounter, minShipsOnPlanet, planetParameters.maxShipsOnPlanet);
            timer = 0;
        }
    }

    private void OnMouseEnter()
    {
        if (isPlayerPlanet)
            SetPlanetColor(cyan);
        
        if (!isPlayerPlanet && isPlayerPlanetSelected)
            SetPlanetColor(white);
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && isPlayerPlanet)
        {
            currentPlayerPlanet = this;
            isPlayerPlanetSelected = true;
            GameManager.instance.GetShipController().SendPlayerPlanetPos(this);
        }

        if (Input.GetMouseButtonDown(0) && isPlayerPlanetSelected && !isPlayerPlanet)
        {
            GameManager.instance.GetShipController().SendNeutralPlanetPos(this);
            isPlayerPlanetSelected = false;
            SetDefaultColor();
            currentPlayerPlanet = null;
        }
    }

    private void OnMouseExit()
    {
        if (isPlayerPlanet && !isPlayerPlanetSelected)
            SetPlanetColor(blue);
        
        if (!isPlayerPlanet && isPlayerPlanetSelected)
            SetPlanetColor(gray);
    }

    private void SetDefaultColor()
    {
        currentPlayerPlanet.SetPlanetColor(blue);
        this.SetPlanetColor(gray);
    }

    private void SetPlanetColor(Color color)
    {
        render.material.color = color;
    }
}
