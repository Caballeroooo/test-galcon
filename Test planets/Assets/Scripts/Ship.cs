﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ship : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;

    private Vector3 endMovePos;

    private float finishPlanetRadius;

    private bool isMove;

    private const float offset = 0.3f;

    private Planet currentFinishPlanet;
    

    void Update()
    {
        if (!isMove)
            return;

        if (Vector3.Distance(transform.position, endMovePos) < finishPlanetRadius + offset)
        {
            isMove = false;
            Destroy(gameObject);
            currentFinishPlanet.ShipHit(1);
        }
    }

    public void StartMove(Planet planet, float radius)
    {
        isMove = true;
        finishPlanetRadius = radius;
        endMovePos = planet.transform.position;
        currentFinishPlanet = planet;
        agent.SetDestination(endMovePos);
    }
}
