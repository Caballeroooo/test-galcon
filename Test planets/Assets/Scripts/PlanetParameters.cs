﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PlanetParameters", menuName = "ScriptableObjects/PlanetParameters", order = 1)]

public class PlanetParameters : ScriptableObject
{
    public int maxCountOfPlanets;
    public float minPlanetDiameter;
    public float maxPlanetDiameter;
    public int maxShipsOnPlanet;
}
