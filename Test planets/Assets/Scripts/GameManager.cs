﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    
    [SerializeField] private MapBuilder mapBuilder;
    
    [SerializeField] private ShipController shipController;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        mapBuilder.Init();
    }
    
    public MapBuilder GetMapBuilder()
    {
        return mapBuilder;
    }

    public ShipController GetShipController()
    {
        return shipController;
    }
}
